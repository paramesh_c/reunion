# Reunion

## Prequisites

1. Node
2. create-react-scripts

## Steps

1. Execute 'npm install' in root directory.
2. Move inside reunionclient folder and execute 'npm install' again.
3. Execute 'npm run app' in root directory.
4. The web application will be running at http://localhost:3000/
