var passport = require('passport');
const config = require('config');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
const redirectDomain = config.get('backendDomain');

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use(
    new GoogleStrategy(
        {
            clientID: config.get('googleClientID'),
            clientSecret: config.get('googleSecret'),
            callbackURL: redirectDomain + 'auth/google/callback'
        },
        function(accessToken, refreshToken, profile, done) {
            var userData = {
                email: profile.emails[0].value,
                name: profile.displayName,
                token: accessToken
            };
            /* console.log(userData); */
            done(null, userData);
        }
    )
);

passport.use(
    new FacebookStrategy(
        {
            clientID: config.get('facebookClientID'),
            clientSecret: config.get('facebookSecret'),
            callbackURL: redirectDomain + 'auth/facebook/callback',
            profileFields: ['displayName', 'email']
        },
        function(accessToken, refreshToken, profile, done) {
            var userData = {
                email: profile.emails[0].value,
                name: profile.displayName,
                token: accessToken
            };
            console.log(userData);

            done(null, userData);
        }
    )
);
