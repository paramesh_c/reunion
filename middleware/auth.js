const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) => {
  let token = req.header('x-auth-token');
  if(!token && req.query && req.query.token)
  {
      token = req.query.token;
  }
  if (!token) {
    return res.status(401).json({ msg: 'Authorization Failure.' });
  } else {
    try {
      const decoded = jwt.verify(token, config.get('jwtSecretToken'));
      req.user = decoded.user;
      next();
    } catch (err) {
      return res.status(401).json({ msg: 'Invalid Token' });
    }
  }
};
