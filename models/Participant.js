const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const participantSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    fullname: {
        type: String,
        required: true,
        max: 70
    },
    age: {
        type: String,
        required: true
    },
    year: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    phoneno: {
        type: String,
        required: true
    },
    spousename: {
        type: String
    },
    spouseage: {
        type: String
    },
    children: [
        {
            name: {
                type: String,
                required: true
            },
            age: {
                type: Number,
                required: true
            }
        }
    ],
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = Profile = mongoose.model('pariticipant', participantSchema);
