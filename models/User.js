const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    role: {
        type: Number,
        required: true
    },
    date: {
        type: String,
        default: Date.now()
    }
});

module.exports = User = mongoose.model('user', UserSchema);
