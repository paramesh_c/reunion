import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './App.css';

import setAuthToken from './utils/setAuthToken';

import loadUser from './services/loaduser';

import Login from './components/Login';
import Form from './components/Form';
import PariticipantList from './components/ParticipantList';

function App() {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [isCompleted, setIsCompleted] = useState(false);
    
    if (localStorage.getItem('token')) {
        setAuthToken(localStorage.getItem('token'));
    }

    const checkUserAuth = async function() {
        const userData = await loadUser();
        if (userData && userData.name) {
            setIsAuthenticated(true);
        }
        setIsCompleted(true);
    };

    useEffect(() => {
        checkUserAuth();
    }, []);

    return (
        <div className='App'>
            <Router>
                <div className='rp-container container-fluid'>
                    <Switch>
                        <Route
                            exact
                            path='/form'
                            render={props => (
                                <Form {...props} isCompleted={isCompleted} isAuthenticated={isAuthenticated} />
                            )}
                        />
                        <Route
                            exact
                            path='/participantlist'
                            render={props => (
                                <PariticipantList
                                    {...props}
                                    isCompleted={isCompleted}
                                    isAuthenticated={isAuthenticated}
                                />
                            )}
                        />
                        <Route
                            path='/'
                            render={props => (
                                <Login {...props} isCompleted={isCompleted} isAuthenticated={isAuthenticated} />
                            )}
                        />
                    </Switch>
                </div>
            </Router>
        </div>
    );
}

export default App;
