import React, { useState, useEffect } from 'react';
import { Redirect, Link } from 'react-router-dom';
import useStateWithCallback from 'use-state-with-callback';

import './Form.css';
import getParticipantForm from '../../services/getparticipantform';
import submitParticipantForm from '../../services/submitParticipantForm';

function Form(props) {
    const [formData, setformData] = useState({
        fullname: '',
        year: new Date().getFullYear(),
        age: '',
        email: '',
        phoneno: '',
        spousename: '',
        spouseage: '',
        children: []
    });
    const [hasSpouse, sethasSpouse] = useState(false);
    const [hasChildren, sethasChildren] = useState(false);
    const [isRegistered, setIsRegistered] = useState(false);
    const [errors, setErrors] = useStateWithCallback([], errors => {
        if (errors.length) {
            window.scrollTo(0, 0);
        }
    });

    let { fullname, year, age, email, phoneno, spousename, spouseage, children } = formData;

    const updateFields = e => {
        setformData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const updateChildFields = (e, index) => {
        if (children[index]) {
            children[index][e.target.name] = e.target.value;
        }
        setformData({
            ...formData,
            children: children
        });
    };

    const handleChildSelection = e => {
        if (e.target.checked && children.length == 0) {
            addChildren();
        }
        sethasChildren(e.target.checked);
    };

    const addChildren = () => {
        children.push({ name: '', age: '' });
        setformData({
            ...formData,
            children: children
        });
    };

    const deleteChildren = index => {
        if (children[index]) {
            children.splice(index, 1);
            setformData({
                ...formData,
                children: children
            });
        }
    };

    const setParticipantFormData = formData => {
        setformData(formData);
        setIsRegistered(true);
        if (formData.spousename && formData.spouseage) {
            sethasSpouse(true);
        }
        if (formData.children && formData.children.length) {
            sethasChildren(true);
        }
    };

    const validateForm = () => {
        let errorMsgs = [];
        if (!fullname) errorMsgs.push('Please enter your full name');
        if (!year) errorMsgs.push('Please enter the year you passed out');
        if (!(age && /^[0-9\+]+$/g.test(age))) errorMsgs.push('Please enter a valid age');
        if (!(email && /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/gim.test(email)))
            errorMsgs.push('Please enter a valid email');
        if (!(phoneno && /^[0-9+-]+$/g.test(phoneno))) errorMsgs.push('Please enter a phone number');
        if (hasSpouse && !spousename) errorMsgs.push("Please enter your spouse's name");
        if (hasSpouse && !spouseage) errorMsgs.push("Please enter your spouse's age");

        if (hasChildren) {
            const childInpError = children.reduce(
                function(errorObj, child) {
                    errorObj.name = child.name ? errorObj.name : 'Please enter your children name';
                    errorObj.age = child.age ? errorObj.age : 'Please enter your children age';
                    return errorObj;
                },
                { name: '', age: '' }
            );
            if (childInpError.name) errorMsgs.push(childInpError.name);
            if (childInpError.age) errorMsgs.push(childInpError.age);
        }
        if (errorMsgs.length) {
            setErrors(errorMsgs);
            return false;
        }
        setErrors([]);
        return true;
    };

    const handleFormSubmission = async () => {
        if (validateForm()) {
            const participantData = formData;
            if (!hasSpouse) {
                participantData.spousename = '';
                participantData.spouseage = '';
                setformData({ spousename: '', spouseage: '', ...formData });
            }
            if (!hasChildren) {
                participantData.children = [];
                setformData({ children: [], ...formData });
            }
            const participantFormData = await submitParticipantForm(participantData);
            if (participantFormData) {
                setParticipantFormData(participantFormData);
                props.history.push('/participantlist');
            }
        }
    };

    const fetchParticipantForm = async () => {
        const participantFormData = await getParticipantForm();
        if (participantFormData) {
            setParticipantFormData(participantFormData);
        }
    };

    useEffect(() => {
        fetchParticipantForm();
    }, []);

    if (props.isCompleted && !props.isAuthenticated) {
        return <Redirect to='/'></Redirect>;
    }

    return (
        <div className='rp-form-content pt-3 pb-3'>
            <div className='row justify-content-center row no-gutters'>
                <div className='col-12 text-center pt-3 pb-3 mt-3'>
                    <h1 className='rp-form-title'>Registration Form</h1>
                </div>
            </div>
            <div className='row justify-content-center no-gutters'>
                {errors.length ? (
                    <div className='col-8 text-justify mb-4 p-3 pl-5 rp-form-error'>
                        <ul>
                            {errors.map((error, index) => {
                                return <li key={index}>{error}</li>;
                            })}
                        </ul>
                    </div>
                ) : (
                    ''
                )}
                <div className='col-8'>
                    <div className='rp-form-container pt-3 pb-3'>
                        <form
                            onSubmit={e => {
                                e.preventDefault();
                                handleFormSubmission(e);
                            }}
                        >
                            <div className='form-row mb-3 justify-content-between'>
                                <div className='form-group col-md-5'>
                                    <label htmlFor='forGroupFullNameInput'>
                                        Full Name <span className='rp-required'>*</span>
                                    </label>
                                    <input
                                        type='text'
                                        className='form-control'
                                        id='forGroupFullNameInput'
                                        placeholder='John Paul'
                                        value={fullname}
                                        minLength='1'
                                        maxLength='80'
                                        name='fullname'
                                        onChange={e => {
                                            updateFields(e);
                                        }}
                                    />
                                </div>
                                <div className='form-group col-md-3'>
                                    <label htmlFor='inputYear' className='text-nowrap'>
                                        Passed out year <span className='rp-required'>*</span>
                                    </label>
                                    <YearInput year={year} updateFields={updateFields}></YearInput>
                                </div>
                                <div className='form-group col-md-2'>
                                    <label htmlFor='inputAge'>
                                        Age <span className='rp-required'>*</span>
                                    </label>
                                    <input
                                        type='number'
                                        maxLength='3'
                                        className='form-control'
                                        id='inputAge'
                                        placeholder='Age'
                                        value={age}
                                        name='age'
                                        onChange={e => {
                                            updateFields(e);
                                        }}
                                    />
                                </div>
                            </div>
                            <div className='form-row mb-3 justify-content-between'>
                                <div className='form-group col-md-5'>
                                    <label htmlFor='inputEmail'>
                                        Email <span className='rp-required'>*</span>
                                    </label>
                                    <input
                                        type='email'
                                        minLength='1'
                                        className='form-control'
                                        id='inputEmail'
                                        placeholder='john@example.com'
                                        value={email}
                                        name='email'
                                        onChange={e => {
                                            updateFields(e);
                                        }}
                                    />
                                </div>
                                <div className='form-group col-md-5'>
                                    <label htmlFor='inputTelephone'>
                                        Phone Number <span className='rp-required'>*</span>
                                    </label>
                                    <input
                                        type='tel'
                                        className='form-control'
                                        id='inputTelephone'
                                        placeholder='+91xxxxxxxxxx'
                                        value={phoneno}
                                        name='phoneno'
                                        onChange={e => {
                                            updateFields(e);
                                        }}
                                    />
                                </div>
                            </div>
                            <div className='custom-control custom-checkbox  mb-2'>
                                <input
                                    type='checkbox'
                                    className='custom-control-input'
                                    checked={hasSpouse}
                                    id='spouseCheckBox'
                                    onChange={e => {
                                        sethasSpouse(e.target.checked);
                                    }}
                                />
                                <label className='custom-control-label' htmlFor='spouseCheckBox'>
                                    Are you accompanied by your spouse ?
                                </label>
                            </div>
                            {hasSpouse && (
                                <div className='form-row mb-3'>
                                    <div className='form-group col-md-5'>
                                        <label htmlFor='spouseNameInput'>
                                            Spouse Name <span className='rp-required'>*</span>
                                        </label>
                                        <input
                                            type='text'
                                            className='form-control'
                                            id='spouseNameInput'
                                            value={spousename}
                                            name='spousename'
                                            onChange={e => {
                                                updateFields(e);
                                            }}
                                        />
                                    </div>
                                    <div className='form-group col-xs-1'>
                                        <label htmlFor='inputSpouseAge'>
                                            Age <span className='rp-required'>*</span>
                                        </label>
                                        <input
                                            type='number'
                                            maxLength='3'
                                            className='form-control'
                                            id='inputSpouseAge'
                                            placeholder='Age'
                                            value={spouseage}
                                            name='spouseage'
                                            onChange={e => {
                                                updateFields(e);
                                            }}
                                        />
                                    </div>
                                </div>
                            )}
                            <div className='custom-control custom-checkbox mb-2'>
                                <input
                                    type='checkbox'
                                    className='custom-control-input'
                                    id='childrenCheckbox'
                                    checked={hasChildren}
                                    onChange={e => {
                                        handleChildSelection(e);
                                    }}
                                />
                                <label className='custom-control-label' htmlFor='childrenCheckbox'>
                                    Do you have children ?
                                </label>
                            </div>
                            {hasChildren &&
                                children.length > 0 &&
                                children.map((child, index) => {
                                    return (
                                        <div className='form-row mb-1' key={index}>
                                            <div className='form-group col-sm-5'>
                                                <label htmlFor={'childNameInput' + index}>
                                                    Child Name <span className='rp-required'>*</span>
                                                </label>
                                                <input
                                                    type='text'
                                                    className='form-control'
                                                    id={'childNameInput' + index}
                                                    placeholder='Donald'
                                                    value={child.name}
                                                    name='name'
                                                    onChange={e => {
                                                        updateChildFields(e, index);
                                                    }}
                                                />
                                            </div>
                                            <div className='form-group col-xs-1'>
                                                <label htmlFor={'inputSpouseAge' + index}>
                                                    Age <span className='rp-required'>*</span>
                                                </label>
                                                <input
                                                    type='number'
                                                    maxLength='3'
                                                    className='form-control'
                                                    id={'inputSpouseAge' + index}
                                                    placeholder='Age'
                                                    value={child.age}
                                                    name='age'
                                                    onChange={e => {
                                                        updateChildFields(e, index);
                                                    }}
                                                />
                                            </div>
                                            <button
                                                className='btn mt-3 text-danger'
                                                onClick={e => {
                                                    e.preventDefault();
                                                    deleteChildren(index);
                                                }}
                                            >
                                                <i className='fa fa-trash'></i>
                                            </button>
                                        </div>
                                    );
                                })}
                            {hasChildren && children.length < 5 && (
                                <div className='form-row mb-3'>
                                    <div className='form-group'>
                                        <button
                                            type='button'
                                            className='btn btn-outline-primary'
                                            onClick={e => {
                                                addChildren();
                                            }}
                                        >
                                            <i className='fa fa-plus'></i> Add
                                        </button>
                                    </div>
                                </div>
                            )}
                            <div className='form-row mb-3 justify-content-center'>
                                <input
                                    type='submit'
                                    value={isRegistered ? 'Update Form' : 'Submit Form'}
                                    className='rp-form-submitbutton btn btn-primary mt-3'
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <Link className='rp_next_btn' to='participantlist'>
                <i className='fa fa-arrow-circle-right'></i>
            </Link>
        </div>
    );
}

function YearInput(props) {
    const options = [],
        currentYear = new Date().getFullYear();
    for (let i = currentYear; i > currentYear - 60; i--) {
        options.push(
            <option key={Math.random()} value={i}>
                {i}
            </option>
        );
    }
    return (
        <select
            id='inputYear'
            className='form-control'
            value={props.year}
            onChange={e => {
                props.updateFields(e);
            }}
            name='year'
        >
            {options}
        </select>
    );
}

export default Form;
