import React from 'react';
import { Redirect } from 'react-router-dom';
import './Login.css';
import googlelogo from '../../assets/google_logo.svg';

function Login(props) {
    if (props.isCompleted && props.isAuthenticated) {
        return <Redirect to='/form'></Redirect>;
    }

    return (
        <div className='rp-login-content row justify-content-center align-items-center no-gutters'>
            <div className='col-8'>
                <div className='row'>
                    <div className='col-12 text-center p-5'>
                        <h1 className='rp-app-title'>The Great Reunion 2020</h1>
                    </div>
                    <div className='col-md-6 mb-3'>
                        <div className='d-flex justify-content-center'>
                            <a href='/auth/google' className='rp-google-button'>
                                <div className='rp-social-icon'>
                                    <img src={googlelogo} alt='logo' />
                                </div>
                                <span>Login with Google</span>
                            </a>
                        </div>
                    </div>
                    <div className='col-md-6 mb-3'>
                        <div className='d-flex justify-content-center'>
                            <a href='/auth/facebook' className='btn rp-facebook-button'>
                                <i className='fa fa-facebook rp-social-icon'></i>
                                <span>Login with Facebook</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;
