import React, { useEffect, useState } from 'react';
import { Redirect, Link } from 'react-router-dom';

import './ParticipantList.css';
import getAllParticipants from '../../services/getAllParticipants';

function ParticipantList(props) {
    const [participants, setParticipants] = useState([]);

    const fetchAllPariticpants = async () => {
        const participantsData = await getAllParticipants();
        setParticipants(participantsData);
    };

    useEffect(() => {
        fetchAllPariticpants();
    }, []);

    if (props.isCompleted && !props.isAuthenticated) {
        return <Redirect to='/'></Redirect>;
    }

    return (
        <div className='rp-list-content pt-3 pb-3'>
            <div className='justify-content-center row no-gutters'>
                <div className='col-12'>
                    <div className='row no-gutters'>
                        <div className='col-12 text-center pt-3 pb-3 mt-3'>
                            <h1 className='rp-list-title'>Participants List</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div className='row justify-content-center no-gutters'>
                <div className='col-10 rp-list-table-container'>
                    <table className='table table-dark table-hover w-100 rp-list-table'>
                        <thead>
                            <tr>
                                <th scope='col'>#</th>
                                <th scope='col'>Name</th>
                                <th scope='col'>Year passed out</th>
                                <th scope='col'>Email</th>
                                <th scope='col'>Mobile number</th>
                                <th scope='col'>Spouse</th>
                                <th scope='col'>Children</th>
                            </tr>
                        </thead>
                        <tbody>
                            {participants &&
                                participants.map((particpant, index) => {
                                    let {
                                        fullname,
                                        year,
                                        age,
                                        email,
                                        phoneno,
                                        spousename,
                                        spouseage,
                                        children
                                    } = particpant;
                                    return (
                                        <tr key={index}>
                                            <th scope='row'>{index + 1}</th>
                                            <td>
                                                {fullname}, {age}
                                            </td>
                                            <td>{year}</td>
                                            <td>{email}</td>
                                            <td>{phoneno}</td>
                                            {spousename && <td>{spousename}, {spouseage}</td>}
                                            <td>
                                                {children.length
                                                    ? children.map((child, _index) => {
                                                          return (
                                                              <span key={_index}>
                                                                  {child.name}, {child.age}
                                                                  <br></br>
                                                              </span>
                                                          );
                                                      })
                                                    : ''}
                                            </td>
                                        </tr>
                                    );
                                })}
                        </tbody>
                    </table>
                </div>
            </div>
            <Link className='rp_back_btn' to='form'>
                <i className='fa fa-arrow-circle-left'></i>
            </Link>
            <a className='rp_download_btn' href={"/api/participant/excel?token=" + localStorage.getItem('token')}>
                <i className="fa fa-download"></i>
            </a>
        </div>
    );
}

export default ParticipantList;
