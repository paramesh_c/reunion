import axios from 'axios';

const getAllParticipants = async () => {
    try {
        const options = {
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const participants = await axios.get('/api/participant/all', options);
        return participants.data;
    } catch (err) {
        /* const errors = err.response.data.error;
            if (errors && errors.length) {
                errors.forEach(error => {
                    dispatch(setAlert(error.msg, 'danger'));
                });
            }
            dispatch({ type: AUTH_ERROR }); */
    }
};

export default getAllParticipants;
