import axios from 'axios';

const getParticipantForm = async () => {
    try {
        const options = {
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const participant = await axios.get('/api/participant/@me', options);
        return participant.data;
    } catch (err) {
        /* const errors = err.response.data.error;
            if (errors && errors.length) {
                errors.forEach(error => {
                    dispatch(setAlert(error.msg, 'danger'));
                });
            }
            dispatch({ type: AUTH_ERROR }); */
    }
};

export default getParticipantForm;
