import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import getQueryStringValue from '../utils/getQueryParam';

const loadUser = async () => {
    const token = getQueryStringValue('token') || localStorage.getItem('token');
    if (token) {
        localStorage.setItem('token', token);
        setAuthToken(token);
        try {
            const options = {
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            const user = await axios.get('/api/user', options);
            return user.data;
        } catch (err) {
            /* const errors = err.response.data.error;
            if (errors && errors.length) {
                errors.forEach(error => {
                    dispatch(setAlert(error.msg, 'danger'));
                });
            }
            dispatch({ type: AUTH_ERROR }); */
        }
    } else {
        /* dispatch({ type: AUTH_ERROR }); */
    }
};

export default loadUser;
