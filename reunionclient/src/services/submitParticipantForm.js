import axios from 'axios';

const submitParticipantForm = async ({ fullname, year, age, email, phoneno, spousename, spouseage, children }) => {
    try {
        const body = JSON.stringify({ fullname, year, age, email, phoneno, spousename, spouseage, children });
        const options = {
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const res = await axios.post('/api/participant', body, options);

        if (res && res.data) {
            return res.data;
        }
        return false;
    } catch (err) {
        return false;
    }
};

export default submitParticipantForm;
