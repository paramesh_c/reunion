const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(
        proxy(['/api/user', '/api/participant/@me', '/api/participant', '/auth/google', '/auth/facebook', '/test'], {
            target: 'http://localhost:5000',
            changeOrigin: true
        })
    );
};
