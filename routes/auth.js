const express = require('express');
const router = express.Router();
const passport = require('passport');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

const User = require('../models/User');

const clientDomain = config.get('clientDomain');

const isRegisteredUser = async function(email) {
    return await User.findOne({ email });
};

const authenticateUser = async function(name, email) {
    let user = await isRegisteredUser(email);
    if (!user) {
        user = new User({
            name,
            email,
            role: 2,
            date: new Date()
        });

        /* console.log(user); */

        try {
            await user.save();
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Internal Server Error');
        }
    }

    let payload = {
        user: {
            id: user.id
        }
    };

    let tokenPromise = new Promise((resolve, reject) => {
        jwt.sign(payload, config.get('jwtSecretToken'), { expiresIn: '1d' }, (err, token) => {
            if (err) {
                reject();
            }
            resolve(token);
        });
    });
    return tokenPromise;
};

/* GET Google Authentication API. */
router.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

router.get(
    '/google/callback',
    passport.authenticate('google', { failureRedirect: clientDomain, session: false }),
    async function(req, res) {
        const { name, email } = req.user;
        try {
            await authenticateUser(name, email).then(
                token => {
                    res.redirect(clientDomain + '?token=' + token);
                },
                () => {
                    res.redirect(clientDomain);
                }
            );
        } catch (err) {
            console.log(err);
            res.redirect(clientDomain);
        }
    }
);

router.get('/facebook', passport.authenticate('facebook', { scope: ['email'] }));

router.get('/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/' }), async function(req, res) {
    const { name, email } = req.user;
    try {
        await authenticateUser(name, email).then(
            token => {
                res.redirect(clientDomain + '?token=' + token);
            },
            () => {
                res.redirect(clientDomain);
            }
        );
    } catch (err) {
        res.redirect(clientDomain);
    }
});

module.exports = router;
