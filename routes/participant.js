const express = require('express');
const { check, validationResult } = require('express-validator');

const excel = require('node-excel-export');


const router = express.Router();

const auth = require('../middleware/auth');

const Participant = require('../models/Participant');

const User = require('../models/User');

// @route GET api/participant/@me
// @desc Get User Registration Details
// @access Private
router.get('/@me', auth, async (req, res) => {
    try {
        const profile = await Participant.findOne({
            user: req.user.id
        });
        if (profile) {
            res.status(200).json(profile);
        } else {
            res.status(404).send('There is no profile for the user');
        }
    } catch (err) {
        res.status(500).send('Server Error');
    }
});

// @route POST api/participant
// @desc Add/Update User Registration Details
// @access Private
router.post(
    '/',
    [
        auth,
        check('fullname', 'Name is required')
            .not()
            .isEmpty(),
        check('age', 'Age is required')
            .not()
            .isEmpty(),
        check('year', 'Pass out year is required')
            .not()
            .isEmpty(),
        check('email', 'Email is required')
            .not()
            .isEmpty(),
        check('phoneno', 'Phone number is required')
            .not()
            .isEmpty()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);
            if (errors.isEmpty()) {
                const { fullname, year, age, email, phoneno, spousename, spouseage, children } = req.body;

                let participantFields = {};

                participantFields.user = req.user.id;
                if (fullname) participantFields.fullname = fullname;
                if (age) participantFields.age = age;
                if (year) participantFields.year = year;
                if (email) participantFields.email = email;
                if (phoneno) participantFields.phoneno = phoneno;
                if (spousename || spousename == '') participantFields.spousename = spousename;
                if (spouseage || spouseage == '') participantFields.spouseage = spouseage;
                if (children) participantFields.children = children;

                let participant = await Participant.findOne({ user: req.user.id });
                if (participant) {
                    participant = await Participant.findOneAndUpdate({ user: req.user.id }, participantFields, {
                        new: true
                    });
                } else {
                    participant = new Participant(participantFields);
                    await participant.save();
                }
                res.status(200).json(participant);
            } else {
                res.status(400).json({ errors: errors.array() });
            }
        } catch (err) {
            res.status(500).send('Server Error' + JSON.stringify(err));
        }
    }
);

// @route Delete api/participant/@me
// @desc Delete Registration Details
// @access Private
router.delete('/@me', auth, async (req, res) => {
    try {
        await Participant.findOneAndRemove({
            user: req.user.id
        });
        res.status(200).json({ msg: 'Your Registration is Deleted' });
    } catch (err) {
        res.status(500).send('Server Error');
    }
});

// @route GET api/participant/all
// @desc List all Registration Details
// @access Private
router.get('/all', auth, async (req, res) => {
    try {
        const particpants = await Participant.find();
        if (particpants) {
            res.status(200).json(particpants);
        } else {
            res.status(200).send('There are no Participants.');
        }
    } catch (err) {
        res.status(500).send('Server Error');
    }
});

// @route GET api/participant/excel
// @desc Export except
// @access Private
router.get('/excel', auth, async function(req, res) {
    try {
        const participants = await Participant.find();
        if (participants && participants.length) {
            const styles = {
                headerDark: {
                    fill: {
                        fgColor: {
                            rgb: 'FFFFFFFF'
                        }
                    },
                    font: {
                        sz: 14,
                        bold: true
                    }
                }
            };
            const specification = {
                id: {
                    displayName: '#',
                    headerStyle: styles.headerDark,
                    width: '4'
                },
                participant_name: {
                    displayName: 'Participant Name',
                    headerStyle: styles.headerDark,
                    width: '30'
                },
                participant_age: {
                    displayName: 'Participant Age',
                    headerStyle: styles.headerDark,
                    width: '3'
                },
                year: {
                    displayName: 'Year Passed Out',
                    headerStyle: styles.headerDark,
                    width: '5'
                },
                email: {
                    displayName: 'Email',
                    headerStyle: styles.headerDark,
                    width: '20'
                },
                phonenumber: {
                    displayName: 'Phone Number',
                    headerStyle: styles.headerDark,
                    width: '20'
                },
                spouse: {
                    displayName: 'Spouse',
                    headerStyle: styles.headerDark,
                    width: '30'
                },
                spouseage: {
                    displayName: 'Spouse Age',
                    headerStyle: styles.headerDark,
                    width: '3'
                },
                children: {
                    displayName: 'Children',
                    headerStyle: styles.headerDark,
                    width: '50'
                }
            };

            let rows = [];

            participants.map(function(participant, index) {
                let row = {
                    id: index + 1,
                    participant_name: participant.fullname,
                    participant_age: participant.age,
                    year: participant.year,
                    email: participant.email,
                    phonenumber: participant.phoneno,
                    spouse: participant.spousename,
                    spouseage: participant.spouseage
                };
                if (participant.children.length) {
                    row['children'] = '';
                    participant.children.map((child, index) => {
                        row['children'] +=
                            (row['children'] && '  \n') +
                            child.name +
                            ', ' +
                            child.age;
                    });
                }
                rows.push(row);
            });

            const report = excel.buildExport([
                {
                    name: 'ParticipantList',
                    specification: specification,
                    data: rows
                }
            ]);

            res.attachment('ParticipantList.xlsx');
            return res.send(report);
        } else {
            res.status(200).send('There are no Participants.');
        }
    } catch (err) {
        console.log(err);
        res.status(500).send('Server Error');
    }
});

module.exports = router;
