const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');

const User = require('../models/User');

// @route GET api/auth
// @desc Get User Details
// @access Private
router.get('/', auth, async (req, res) => {
    try {
        let user = await User.findById(req.user.id);
        res.status(200).json(user);
    } catch (err) {
        res.status(500).send('Server Error');
    }
});

module.exports = router;
