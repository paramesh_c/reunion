const express = require('express');
const passport = require('passport');
const dbConnection = require('./config/dbconnection');
const path = require('path');

const PORT = process.env.PORT || 5000;

const app = express();

//Connect to Mongo DB
dbConnection();

app.use(express.json({ extended: false }));
app.use(express.urlencoded({ extended: false }));
app.use(passport.initialize());

require('./config/passport');


app.use('/auth', require('./routes/auth'));
app.use('/api/user', require('./routes/user'));
app.use('/api/participant', require('./routes/participant'));

app.listen(PORT, () => {
    console.log('Server started on port' + PORT);
});

if(process.env.NODE_ENV === "production")
{
    app.use(express.static('reunionclient/build'));
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'reunionclient', 'build', 'index.html'));
    });
}
